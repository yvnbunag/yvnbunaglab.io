/**
 * Type to allow types to be set to null
 *
 * @typeParam Type - type to be allowed as null
 */
type Nullable<Type> = Type | null
