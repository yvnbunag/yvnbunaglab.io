declare namespace Heading {
  type StringLevels = '1' | '2' | '3' | '4' | '5' | '6'
  type NumberLevels = 1 | 2 | 3 | 4 | 5 | 6
  type Levels = StringLevels | NumberLevels
}
