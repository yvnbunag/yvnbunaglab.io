import heading from '~/components/wrappers/heading.vue'
import responsiveContainer from '~/components/wrappers/responsive-container.vue'

export {
  heading,
  responsiveContainer,
}
