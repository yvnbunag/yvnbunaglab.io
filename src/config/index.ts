import * as profile from '~/config/profile'
import { icons } from '~/config/icons'
import { technologies } from '~/config/technologies'
import { pages } from '~/config/pages'

export {
  profile,
  icons,
  technologies,
  pages,
}
